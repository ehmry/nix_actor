# Reactive Nix

Last month at the Nix [Volga Sprint](https://volgasprint.org/) the [nix-actor frontend](https://git.syndicate-lang.org/ehmry/nix_actor) for the [Syndicated actor model](https://syndicate-lang.org) was reduced to a new abstract interface for a more general form of Nix evaluation.

While there are multiple efforts to create or maintain independent implementations of the Nix evaluator and the Nix store, the `nix-actor` is an experiment in high-level interaction with the evaluator and store using Syndicate as a language-independent protocol.

## Previous work

The frontend started as an experiment in proxying the protocol that passes between the standard frontends and the Nix worker daemon. Shortly thereafter a C API arrived for Nix and the experiment expanded to cover evaluation of Nix expressions. Since then the worker protocol proxy was dropped and the evaluation interface shifted towards a model centered on the concept of a "repo".

Historically Nix expressions have mostly congealed around the Nixpkgs monorepo. Whether or not Nixpkgs is an effective monorepo or if the monorepo is an effective model for the package collection is up for debate, but it seemed like an obviousy way to structure evaluation in a Syndicate frontend. The frontend would [assert](https://synit.org/book/glossary.html#assertion) a repo [capability](https://synit.org/book/glossary.html#capability) in response to an assertion of a file-system path to import. This repo capability would represent an evaulation state and emulate [dataspace](https://synit.org/book/glossary.html#dataspace) observations by unthunking Nix values to [Preserves values](https://synit.org/book/glossary.html#preserves).

## Current Evaluation Interface

The current iteration of the frontend is much simpler. Initially a capability to an empty evaluation state is asserted by a [gatekeeper](https://synit.org/book/protocols/syndicate/gatekeeper.html) in response to some optional evaluation parameters. The evaluation state is advanced by assertions in the form of `<eval «expr» «args» «result»>` to which responses are returned via a `result` capability.

In this scheme the `expr` field of the `eval` [record](https://synit.org/book/glossary.html#record) is a string of Nix code that parses to a function that returns a function. The first function is called with the root Nix value of the evaluation state. The second function is called with the Preserves value at the `args` field converted to a Nix value. This implies that the type of the `args` field must be within the intersection of Preserves and Nix types. On one hand this excludes records (`<…>`), byte-strings, and sets and on the other it excludes functions and lazy values.

To import Nixpkgs one might assert

    <eval "_: _: import <nixpkgs> { }" <null> #:… >

Both arguments here are ignored. The initial Nix value of an evaluation state is `nil` and the `args` field is set to the Preserves equivalent.

This causes a new capability to the root of the Nixpkgs attribute set be asserted within an `<ok …>` record to the capability at the `result` field, otherwise an `<error …>` record is asserted.

To import with cross-compilation arguments we could assert

    <eval "_: import <nixpkgs>" { crossSystem: "mips64-linux" } #:… >

In this case only the first argument is discarded and the `args` field is passed on to the Nixpkgs priming function.

The result of either example is a capability which can be queried using dataspace observations. To capture the version of the `preserves-tools` package a [pattern](https://synit.org/book/glossary.html#dataspace-pattern) in the following form could be used:

    { preserves-tools: { version: ? } }

Observations pass through Nix attribute sets and capture non-function Nix values as Preserves values. Observing a package attrset directly poses a problem however, as an package attrset will contain a closure of all of the dependencies and their attrsets, which is likely a cyclic graph of attrsets. The solution is simple and aligned with Nix sematics. The observation of an attrset with an `outPath` attribute yields its `outPath` value. This means all derviations (packages) are observed directly as the Nix store path of their realisation. Observations of these two patterns yield identical results:

    { preserves-tools: { outPath: ? } }
    { preserves-tools: ? }

This leads to the question of how to go from an evaluation value to a store path that actually exists on the file-system.

    <realise-string «result»>

The `realise-string` record is asserted with a `result` receiver capability to an evaluation state that has been stepped to a string value and a result value is asserted when the necessary builders have run to competion. To step a Nixpkgs state to the path of a build of the `preserves-tools` the following could be asserted:

    <eval "pkgs: pname: pkgs.lib.getExe (builtins.getAttr pname pkgs)" "preserves-tools" #:…>

## Copying store paths

In the case of software packages a Nix evaluation will usually result in a store path that has already been built and can be copied from a remote cache. The current implementation of `nix-actor` is a thin wrapper over the Nix library so it inherits the implicit substitution behavior of the local Nix installation. This is to say the `<realise-string …>` assertion will typically fetch the bulk of its closure from the Nix cache.

In the case of cross-compilation the evaluation and realisation of store paths happens on machine distinct from the machine where the store paths are intended to be used. To satisfy the remote evaluation scenario the `nix-actor` supports explicit replication of store paths. When the `nix-actor` gatekeeper is requested to resolve `<nix-store { storeUri: "…" }>` it responds with an entity that represents a Nix store decoupled from an evaluation state. See the [Nix documentation](https://nix.dev/manual/nix/2.24/store/types/) for descriptions of the supported `storeUri` variants.

Both the evaluation entities and store entities respond to the following assertion type:

    <replicate «target-store» «store-path-string» «result»>

The corresponding behavior is to copy the `store-path-string` from a store that contains it to a store that does not, regardless of whether the assertion is made to the store with or the store without. The interface to the store entity is a subset of the interface to the evaluation entity, overlapping at the `<replicate …>` assertion.

## Speculative use-cases

In the NixOS deployment model a NixOS system is a total description that is evaluated to a single closure of build artifacts and transitions between declared states is made as atomic as possible. This can be called a congruent deployment model.

Driving Nix with Syndicate quickly leads to convergent deployment model. Nix expressions are evaluated on demand in an iterative process that moves a system towards a destination state. Is this useful or a regression in practice?

The most obvious use case for convergence is system orchestration. A simple case of orchestration would be a "road-warrior" or "digital nomad" with a laptop and a single remote server. In the congruent model the two systems might be delared as complete NixOS system descriptions. Both systems might build from a common evaluation origin, such as a Nixpkgs commit and a shared configuration repo. In a convergent deployment both sides may not necessarily have a common origin nor a system state that perisists across reboots.

Imagine the common case where a laptop requires an HTTP proxy that terminates at the server location and this proxy is only used by the laptop. The proxy therefore only needs to be running while the laptop is online and capable of connecting to the server, and thus the proxy configuration should be owned by the laptop and not the server.

Assuming there is a secure IP tunnel between the two machines we can run a syndicate-server instance on the remote machine and listen for connections to a configuration dataspace. The configuration file would look something like this:

    # Server-side bind of a TCP port and a sturdyref.
    <require-service <relay-listener <tcp "10.0.0.1" 2048> $gatekeeper>>
    <bind <ref { oid: "the server oid" key: #x"ffffffffffffffff" }> $config #f>

The laptop also runs a syndicate-server. The laptop connects to and acquires the server configuration dataspace when locally requested:

    let ?resolveServer = <resolve-path <route
        [<tcp "10.0.0.1" 2048>]
        <ref {oid: "the server oid", sig: #[gQN92RzGGn5u8cn11JNvbA]}>>>

    # The laptop side resolves the server configuration
    # dataspace when locally requested.
    $config ? <q <host server>>
    [
      $config += <q $resolveServer>
      $config ? <a $resolveServer <ok <resolved-path _ _ ?remote>>>
      [
        $config += <a <host server> <ok $remote>>
      ]
    ]

The laptop does some Nix evaluation at the server to get a path to a proxy binary and generates a configuration file:

    # First delare the text of the proxy configuration file.
    let ?configText = "
    Allow 10.0.0.0/8
    DisableViaHeader yes
    Port 8888
    "

    # Query local configuration for the server config dataspace.
    $config += <q <host server>>
    $config ? <a <host server> <ok ?server>>
    [
      # Run the proxy daemon once we converge on its description.
      $server += <run-service <daemon tinyproxy>>

      # The server is pre-configured to reply to `nixpkgs-get-exe` queries.
      $server += <q <nixpkgs-get-exe { } "tinyproxy">>
      $server ? <a <nixpkgs-get-exe { } "tinyproxy"> <ok ?exe>>
      [
        # The server is also pre-configured to reply to `writeFile` queries.
        $server += <q <writeFile "tinyproxy.conf" $configText>>
        $server ? <a <writeFile "tinyproxy.conf" $configText> <ok ?configPath>>
        [
          # Assert to the server a descripton of the proxy daemon.
          # It can now be started.
          $server += <daemon tinyproxy [ $exe "-d" "-c" $configPath ]>
        ]
      ]
    ]

That was the high-level interaction between the laptop and server. Now for the implementation of `nixpkgs-get-exe` and `writeFile` at the server:

    # Respond to requests for a top-level evaluation of the Nixpkgs.
    # The `detail` field is for arguments to the top-level function.
    ? <q <nixpkgs ?detail> >
    [
      <require-service <service nix {}>>
      ? <service-object <service nix {}> <ok ?obj>>
      [
        $obj += <eval
            "_: import /nix/var/nix/profiles/per-user/root/channels/nixos"
            $detail
            # The final field is set to a capability attenuation that rewrites
            # a result assertion into an answer to the original query.
            <* $config [ <rewrite ?resp <a <nixpkgs $detail> $resp>> ]>
          >
      ]
    ]

    # Answer requests for `nixpkgs-get-exe` where `pkgName` is the name
    # of a package in the Nixpkgs attribute set and `detail` is how Nixpkgs
    # is initially imported.
    ? <q <nixpkgs-get-exe ?detail ?pkgName>>
    [
      <q <nixpkgs $detail> >
      ? <a <nixpkgs $detail> <ok ?nixpkgs>>
      $nixpkgs
      [
        <eval
            "pkgs: pkgName: pkgs.lib.getExe (builtins.getAttr pkgName pkgs)"
            $pkgName
            # Rewrite a success assertion into an assertion that triggers realisation.
            <* $config [
                <rewrite
                    <ok ?resp>
                    <q <nix-realise <nixpkgs-get-exe $detail $pkgName> $resp>>
                  >
              ]>
          >
      ]
    ]

    # Service requests for `writeFile`.
    # Using an evaluator builtin is sufficient.
    ? <q <writeFile ?fileName ?fileText>>
    [
      ? <service-object <service nix {}> <ok ?nix>>
      [
        $nix += <eval
            "_: { name, text }: builtins.toFile name text"
            { name: $fileName text: $fileText }
            # Rewrite a success assertion into an assertion that triggers realisation.
            <* $config [
                <rewrite
                    <ok ?resp>
                    <q <nix-realise <writeFile $fileName $fileText> $resp>>
                  >
              ]>
          >
      ]
    ]

    # Respond to nix-realise requests.
    ? <q <nix-realise ?qeury ?nix>>
    [
      $nix <realise-string <* $config [ <rewrite ?result <a <nix-realise $qeury $nix> $result>> ]>>
    ]

    # Reflect the nix-realise requests with the nix-realise label removed.
    ? <a <nix-realise ?query> ?result>
    [
      <a $query $result>
    ]

The evaluation work could also be done on the laptop side, but would incure more latency as the intermediary evaluation capabilties would need to make round-trips between laptop and server.

The configuration language used here obviously leaves something to be desired, but here we are orchestrating a remote service with on-demand Nix evaluation rather than a "rebuild the world" state transition at the server.

## Future work

The `nix-actor` utility currently works in isolation when it could also exploit the Syndicate protocol to implement cooperative build caches and build worker pools.

Replicating store paths by serializing through the Syndicate protocol is also a possibility, but not a strongly motivated one. This would be useful in the case of machine that are unwilling or unable to a full Nix implementation, for example, machines with operation systems that lack UNIX semantics.

A broader and more interesting question would be how to implement a convergent variant of NixOS. This effectively depends on building a [Syndicate compatible base system](https://synit.org/) using Nix in congruent mode.
