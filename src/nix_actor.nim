# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[options, tables],
  pkg/preserves,
  pkg/syndicate,
  pkg/syndicate/[gatekeepers, patterns],
  ./nix_actor/[entities, evaluators, nix_api, stores],
  ./nix_actor/schemas/[evaluators, stores]

proc echo(args: varargs[string, `$`]) {.used.} =
  stderr.writeLine(args)

proc bootActor*(turn: Turn; relay: Cap) =
  initLibstore()
  initLibexpr()

  let gk = spawnGatekeeper(turn, relay)

  gk.serve do (turn: Turn; step: StoreResolveStep) -> rpc.Result:
    newStoreEntity(turn, step.detail).self.resultOk

  gk.serve do (turn: Turn; step: EvalResolveStep) -> rpc.Result:
    newEvalEntity(turn, step.detail).self.resultOk

when isMainModule:
  import syndicate/relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn, bootActor)
