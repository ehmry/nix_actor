# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[options],
  pkg/preserves,
  pkg/preserves/sugar,
  pkg/syndicate,
  pkg/syndicate/[patterns],
  ./[entities, nix_api, nix_values],
  ./schemas/[common, evaluators, stores]

proc echo(args: varargs[string, `$`]) {.used.} =
  stderr.writeLine(args)

type
  Value = preserves.Value

  EvalEntity {.final.} = ref object of NixEntity
    state: EvalState
    root: NixValue

proc newEvalEntity*(turn: Turn; detail: EvalResolveDetail): EvalEntity =
  ## Create an initial evaluation state.
  let entity = EvalEntity(
      store: openStore(detail.storeUri.get("auto"), detail.storeParams)
    )
  if detail.lookupPath.isSome:
    entity.state = newState(entity.store, detail.lookupPath.get)
  else:
    entity.state = newState(entity.store)
  entity.root = entity.state.initNull()
  entity.self = turn.newCap(entity)
  entity.self.relay.onStop do (turn: Turn):
    decref(entity.root)
    entity.state.close()
    entity.store.close()
  entity

proc newChild(parent: EvalEntity; turn: Turn; val: NixValue): EvalEntity =
  ## Create a child entity for a given root value.
  let entity = EvalEntity(
      store: parent.store,
      state: parent.state,
      root: val
    )
  turn.inFacet do (turn: Turn):
    entity.facet = turn.facet
    entity.self = newCap(turn, entity)
    entity.self.relay.onStop do (turn: Turn):
      decref(entity.root)
  entity

proc serve(entity: EvalEntity; turn: Turn; obs: Observe) =
  ## Dataspace emulation.
  var
    analysis = analyse(obs.pattern)
    captures = newSeq[Value](analysis.capturePaths.len)
  block stepping:
    for i, path in analysis.constPaths:
      var v = entity.state.step(entity.root, path)
      if v.isNone or v.get != analysis.constValues[i]:
        let null = initRecord("null")
        for v in captures.mitems: v = null
        break stepping
    for i, path in analysis.capturePaths:
      var v = entity.state.step(entity.root, path)
      if v.isSome:
        captures[i] = v.get.unthunkAll
      else:
        captures[i] = initRecord("null")
  publish(turn, Cap obs.observer, captures)

proc serve(entity: EvalEntity; turn: Turn; r: RealiseString) =
  tryPublish(turn, r.result.Cap):
    var str = entity.state.realiseString(entity.root)
    publishOk(turn, r.result.Cap, %str)

proc serve(entity: EvalEntity; turn: Turn; e: Eval) =
  tryPublish(turn, e.result.Cap):
    var expr = entity.state.evalFromString(e.expr)
    expr = entity.state.apply(expr, entity.root)
    expr = entity.state.apply(expr, e.args.toNix(entity.state))
    publishOk(turn, e.result.Cap, entity.newChild(turn, expr).self.toPreserves)

method publish(entity: EvalEntity; turn: Turn; assRef: AssertionRef; h: Handle) =
  var
    ass: AssertionToEvaluator
    observe: Observe
  if ass.fromPreserves(assRef.value):
    case ass.orKind
    of AssertionToEvaluatorKind.Eval:
      serve(entity, turn, ass.eval)
    of AssertionToEvaluatorKind.RealiseString:
      serve(entity, turn, ass.realisestring)
    of AssertionToEvaluatorKind.Replicate:
      serve(entity, turn, ass.replicate)
  elif observe.fromPreserves(assRef.value):
    serve(entity, turn, observe)
  else:
    when not defined(release):
      echo "unhandled assertion ", assRef.value
