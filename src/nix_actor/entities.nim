# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[options, strutils, tables],
  pkg/preserves,
  pkg/preserves/sugar,
  pkg/syndicate,
  pkg/syndicate/[patterns, relays],
  ./nix_api,
  ./schemas/[common, stores]

proc echo(args: varargs[string, `$`]) {.used.} =
  stderr.writeLine(args)

type Value = preserves.Value

template tryPublish*(turn: Turn, cap: Cap; body: untyped) =
  try: body
  except CatchableError as err:
    when not defined(release):
      stderr.writeLine err.msg
    publish(turn, cap, Error(detail: %err.msg))

proc publishOk*(turn: Turn; cap: Cap, v: Value) =
  publish(turn, cap, Ok(value: v))

proc publishError*(turn: Turn; cap: Cap, v: Value) =
  publish(turn, cap, Error(detail: v))

proc unembedEntity*(emb: EmbeddedRef; E: typedesc): Option[E] =
  if emb of Cap and emb.Cap.target of E:
    result = emb.Cap.target.E.some

proc unembedEntity*(v: Value; E: typedesc): Option[E] =
  if v.isEmbeddedRef:
    result = v.embeddedRef.unembedEntity(E)

proc openStore*(uri: string; params: Option[AttrSet]): Store =
  var pairs: seq[string]
  if params.isSome:
    var  i: int
    pairs.setLen(params.get.len)
    for (key, val) in params.get.pairs:
      pairs[i] = $key & "=" & $val
      inc i
  openStore(uri, pairs)

type
  NixEntity* = ref object of Entity
    self*: Cap
    store*: Store

proc serve*(entity: NixEntity; turn: Turn; rep: Replicate) =
  tryPublish(turn, rep.result.Cap):
    var
      target: Store
      otherEntity = rep.target.unembedEntity(NixEntity)
    if otherEntity.isSome:
      target = otherEntity.get.store
    if target.isNil:
      publishError(turn, rep.result.Cap, %"cannot replicate with target")
    else:
      if entity.store.isValidPath(rep.storePath):
        entity.store.copyClosure(target, rep.storePath)
          # path exists at entity
      else:
        target.copyClosure(entity.store, rep.storePath)
          # path hopefully exists at target
      publishOk(turn, rep.result.Cap, %rep.storePath)
