# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  pkg/preserves,
  pkg/preserves/sugar,
  pkg/syndicate,
  ./[entities, nix_api],
  ./schemas/[common, stores]

proc echo(args: varargs[string, `$`]) {.used.} =
  stderr.writeLine(args)

type
  StoreEntity {.final.} = ref object of NixEntity

proc newStoreEntity*(turn: Turn; detail: StoreResolveDetail): StoreEntity =
  let entity = StoreEntity(store: openStore(detail.storeUri, detail.storeParams))
  entity.self = turn.newCap(entity)
  entity.self.relay.onStop do (turn: Turn):
    entity.store.close()
  entity

proc serve*(entity: StoreEntity; turn: Turn; checkPath: CheckStorePath) =
  tryPublish(turn, checkPath.valid.Cap):
    var v = entity.store.isValidPath(checkPath.path)
    publish(turn, checkPath.valid.Cap, initRecord("ok", %v))

method publish(entity: StoreEntity; turn: Turn; assRef: AssertionRef; h: Handle) =
  var ass: AssertionToStore
  if ass.fromPreserves(assRef.value):
    case ass.orKind
    of AssertionToStoreKind.CheckStorePath:
      entity.serve(turn, ass.checkStorePath)
    of AssertionToStoreKind.Replicate:
      entity.serve(turn, ass.replicate)
  else:
    when not defined(release):
      echo "unhandled assertion ", assRef.value
