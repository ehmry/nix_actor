
import
  preserves, common, stores, std/options

type
  EvalResolveDetailLookupPath* = Option[seq[string]]
  EvalResolveDetailStoreUri* = Option[string]
  `EvalResolveDetail`* {.preservesDictionary.} = object
    `lookupPath`*: Option[seq[string]]
    `storeParams`*: Option[common.AttrSet]
    `storeUri`*: Option[string]

  Eval* {.preservesRecord: "eval".} = object
    `expr`*: string
    `args`*: Value
    `result`* {.preservesEmbedded.}: EmbeddedRef

  LookupPathKind* {.pure.} = enum
    `lookupPath`, `absent`
  LookupPathLookupPath* {.preservesDictionary.} = object
    `lookupPath`*: seq[string]

  LookupPathAbsent* {.preservesDictionary.} = object
  
  `LookupPath`* {.preservesOr.} = object
    case orKind*: LookupPathKind
    of LookupPathKind.`lookupPath`:
        `lookuppath`*: LookupPathLookupPath

    of LookupPathKind.`absent`:
        `absent`*: LookupPathAbsent

  
  AssertionToEvaluatorKind* {.pure.} = enum
    `Eval`, `RealiseString`, `Replicate`
  `AssertionToEvaluator`* {.preservesOr.} = object
    case orKind*: AssertionToEvaluatorKind
    of AssertionToEvaluatorKind.`Eval`:
        `eval`*: Eval

    of AssertionToEvaluatorKind.`RealiseString`:
        `realisestring`*: RealiseString

    of AssertionToEvaluatorKind.`Replicate`:
        `replicate`*: stores.Replicate

  
  StoreParamsKind* {.pure.} = enum
    `storeParams`, `absent`
  StoreParamsStoreParams* {.preservesDictionary.} = object
    `storeParams`*: common.AttrSet

  StoreParamsAbsent* {.preservesDictionary.} = object
  
  `StoreParams`* {.preservesOr.} = object
    case orKind*: StoreParamsKind
    of StoreParamsKind.`storeParams`:
        `storeparams`*: StoreParamsStoreParams

    of StoreParamsKind.`absent`:
        `absent`*: StoreParamsAbsent

  
  RealiseString* {.preservesRecord: "realise-string".} = object
    `result`* {.preservesEmbedded.}: EmbeddedRef

  StoreUriKind* {.pure.} = enum
    `storeUri`, `absent`
  StoreUriStoreUri* {.preservesDictionary.} = object
    `storeUri`*: string

  StoreUriAbsent* {.preservesDictionary.} = object
  
  `StoreUri`* {.preservesOr.} = object
    case orKind*: StoreUriKind
    of StoreUriKind.`storeUri`:
        `storeuri`*: StoreUriStoreUri

    of StoreUriKind.`absent`:
        `absent`*: StoreUriAbsent

  
  EvalResolveStep* {.preservesRecord: "nix".} = object
    `detail`*: EvalResolveDetail

proc `$`*(x: EvalResolveDetail | Eval | LookupPath | AssertionToEvaluator |
    StoreParams |
    RealiseString |
    StoreUri |
    EvalResolveStep): string =
  `$`(toPreserves(x))

proc encode*(x: EvalResolveDetail | Eval | LookupPath | AssertionToEvaluator |
    StoreParams |
    RealiseString |
    StoreUri |
    EvalResolveStep): seq[byte] =
  encode(toPreserves(x))
