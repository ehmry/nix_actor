
import
  preserves, common, std/options

type
  StoreResolveDetailStoreUri* = string
  `StoreResolveDetail`* {.preservesDictionary.} = object
    `storeParams`*: Option[common.AttrSet]
    `storeUri`*: string

  StoreParamsKind* {.pure.} = enum
    `storeParams`, `absent`
  StoreParamsStoreParams* {.preservesDictionary.} = object
    `storeParams`*: common.AttrSet

  StoreParamsAbsent* {.preservesDictionary.} = object
  
  `StoreParams`* {.preservesOr.} = object
    case orKind*: StoreParamsKind
    of StoreParamsKind.`storeParams`:
        `storeparams`*: StoreParamsStoreParams

    of StoreParamsKind.`absent`:
        `absent`*: StoreParamsAbsent

  
  CheckStorePath* {.preservesRecord: "check-path".} = object
    `path`*: string
    `valid`* {.preservesEmbedded.}: EmbeddedRef

  AssertionToStoreKind* {.pure.} = enum
    `CheckStorePath`, `Replicate`
  `AssertionToStore`* {.preservesOr.} = object
    case orKind*: AssertionToStoreKind
    of AssertionToStoreKind.`CheckStorePath`:
        `checkstorepath`*: CheckStorePath

    of AssertionToStoreKind.`Replicate`:
        `replicate`*: Replicate

  
  Replicate* {.preservesRecord: "replicate".} = object
    `target`* {.preservesEmbedded.}: EmbeddedRef
    `storePath`*: string
    `result`* {.preservesEmbedded.}: EmbeddedRef

  StoreResolveStep* {.preservesRecord: "nix-store".} = object
    `detail`*: StoreResolveDetail

proc `$`*(x: StoreResolveDetail | StoreParams | CheckStorePath |
    AssertionToStore |
    Replicate |
    StoreResolveStep): string =
  `$`(toPreserves(x))

proc encode*(x: StoreResolveDetail | StoreParams | CheckStorePath |
    AssertionToStore |
    Replicate |
    StoreResolveStep): seq[byte] =
  encode(toPreserves(x))
