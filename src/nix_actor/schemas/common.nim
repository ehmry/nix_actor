
import
  preserves, std/tables

type
  Error* {.preservesRecord: "error".} = object
    `detail`*: Value

  AttrSet* = Table[Symbol, Value]
  Ok* {.preservesRecord: "ok".} = object
    `value`*: Value

  ResultKind* {.pure.} = enum
    `Ok`, `Error`
  `Result`* {.preservesOr.} = object
    case orKind*: ResultKind
    of ResultKind.`Ok`:
        `ok`*: Ok

    of ResultKind.`Error`:
        `error`*: Error

  
proc `$`*(x: Error | AttrSet | Ok | Result): string =
  `$`(toPreserves(x))

proc encode*(x: Error | AttrSet | Ok | Result): seq[byte] =
  encode(toPreserves(x))
